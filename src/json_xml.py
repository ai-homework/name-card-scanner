# -*- coding: utf-8 -*-
"""
@author: iseyu
"""
import os
import json
import xml.etree.ElementTree as ET
import cv2


def __indent(elem, level=0):
    i = "\n" + level*"\t"
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "\t"
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            __indent(elem, level+1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i

def get_bb_minmax(namebbox):
    new_list=[]
    for item in namebbox:
        new_list.append((float(item.split(",")[0]),float(item.split(",")[1])))
    xmax=max(new_list,key=lambda x: x[0])[0]
    ymax=max(new_list,key=lambda x: x[1])[1] 
    xmin=min(new_list,key=lambda x: x[0])[0]
    ymin=min(new_list,key=lambda x: x[1])[1]
    loca_bb=[xmax,xmin,ymax,ymin]
    
    return loca_bb


def read_json(json_file):
    json_file=open(json_file,'r')
    for line in json_file.readlines():
        dic=json.loads(line)
        namebbox=dic['nameBoundingBox']
        if namebbox!=[]:
            namebbox=get_bb_minmax(namebbox)
        titlebbox=dic['titleBoundingBox']
        if titlebbox!=[]:
            titlebbox=get_bb_minmax(titlebbox)
        companybbox=dic['companyBoundingBox']
        if companybbox!=[]:
            companybbox=get_bb_minmax(companybbox)
    return [namebbox,titlebbox,companybbox]

def create_xml(img_info,json_info,json_file):
    xml_folder_name=r'/home/xyq/yolo3/namecard/xmls/'
    node_root = ET.Element('annotation')       # 创建节点
    tree = ET.ElementTree(node_root)     # 创建文档
    node_size=ET.SubElement(node_root,"size")
    node_width =ET.SubElement(node_size, 'width')
    node_width.text = str(img_info[0])
    node_height =ET.SubElement(node_size, 'height')
    node_height.text = str(img_info[1])
    node_depth =ET.SubElement(node_size, 'depth')
    node_depth.text = str(img_info[2])
    name_list=['nameBoundingBox','titleBoundingBox','companyBoundingBox']
    for i in range(3):
        tree = ET.ElementTree(node_root)
        node_size=ET.SubElement(node_root,"object")
        node_name =ET.SubElement(node_size, 'name')
        node_name.text=name_list[i]
        node_bbox =ET.SubElement(node_size, 'bndbox')
        if json_info[i]==[]:
            node_diff =ET.SubElement(node_size, 'difficult')
            node_diff.text="1"
            continue
        node_bbox_xx=ET.SubElement(node_bbox,"xmax")
        node_bbox_xx.text=str(json_info[i][0])
        node_bbox_xx=ET.SubElement(node_bbox,"xmin")
        node_bbox_xx.text=str(json_info[i][1])
        node_bbox_xx=ET.SubElement(node_bbox,"ymax")
        node_bbox_xx.text=str(json_info[i][2])
        node_bbox_xx=ET.SubElement(node_bbox,"ymin")
        node_bbox_xx.text=str(json_info[i][3])
        node_diff =ET.SubElement(node_size, 'difficult')
        node_diff.text="0"

    __indent(node_root) 
    xml_path=os.path.join(xml_folder_name,"{}.xml".format(json_file.split(".")[0]))        
    tree.write(xml_path, encoding='utf-8', xml_declaration=True)
    
def json2xml():
    json_folder_name=r'/home/xyq/yolo3/namecard/jsons/'
    img_folder=r'/home/xyq/yolo3/namecard/imgs/'
    for json_file in os.listdir(json_folder_name):
        if ".json" in json_file:
            json_path=os.path.join(json_folder_name,json_file)
            all_bbox=read_json(json_path)
            img_path=os.path.join(img_folder,"{}.bmp".format(json_file.split(".")[0]))
            img=cv2.imread(img_path)
            img_info=img.shape
            create_xml(img_info,all_bbox,json_file)


if __name__=='__main__':
    json2xml()
# -*- coding: utf-8 -*-
"""
Created on Tue Dec  3 11:02:24 2019

@author: iseyu
"""

import json
from PIL import Image
import cv2
import numpy as np
import base64
import io
import os



def string2RGB(string_data):
    imgdata=base64.b64decode(str(string_data))
    img=Image.open(io.BytesIO(imgdata))
    img_rgb=cv2.cvtColor(np.array(img),cv2.COLOR_BGR2RGB)
    return img_rgb
    



json_file_location=r'C:\Users\iseyu\Desktop\namecard\cards.json\cards.json'
imgs_save_folder=r'C:\Users\iseyu\Desktop\namecard\imgs'
label_save_foler=r'C:\Users\iseyu\Desktop\namecard\labels'

'''
with open(json_file,'r') as json_f:
    json_data=json.load(json_f)
'''
json_file=open(json_file_location,'r')
texts=[]
name_index=1
for line in json_file.readlines():
    dic=json.loads(line)
    img=dic['imageString']
    img=string2RGB(img)
    name=dic['name']
    
    #save imgs
    img_save_path=os.path.join(imgs_save_folder,'{}.bmp'.format(name_index))
    cv2.imwrite(img_save_path,img)
    
    #save jsons
    json_save_path=os.path.join(label_save_foler,'{}.json'.format(name_index))
    json_dump=json.dumps(dic)
    f=open(json_save_path,'w')
    f.write(json_dump)
    f.close()
    name_index+=1
    






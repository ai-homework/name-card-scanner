# Namecard scanner homework
----
## Objective
The objective of the project has been given to you in a pdf file. Otherwise, you may also refer to the 'Homework Assignment' in the root directory.

## Sample project
To help you get started, instructions have been provided to you on how to train a model using * **YOLO** *

Refer to the instructions given by 'yolo_namecard.rtf'. The dataset containing the labelled scanned business cards are given in the 'cards.json' file provided in the same folder.

If in doubt, feel free to send your enquiry here: < bilguun.batbold@nus.edu.sg>